WaterSense Summer School

When: 23 - 25 February 2022, 9:00 am - 5 pm.

Where: - Online: Link to join Webinar https://uni-sydney.zoom.us/j/85899492935

- In-person: The University of Sydney: Room J12.01.114. The School of Information Technologies. SIT Computer Lab 114

![program.png](./program.png)

Part 1 1_startup_exercises:
We are going to run the Jupyter Notebook tutorials located in the files 0_prep_material and 1_startup_exercises (with exception of Introduction_DEA_Sandbox.ipynb) with Binder
Binder is a cloud-based service that opens notebooks (located in a public repository) in an executable environment, making your the immediately reproducible.

https://mybinder.org/v2/gl/usyd_hydrolab%2Fteaching%2Fsummerschool_2022/HEAD

Part 2 1_startup_exercises:
We are going to run the Jupyter Notebook Introduction_DEA_Sandbox.ipynb in DEA Sandbox platform. https://app.sandbox.dea.ga.gov.au/ 

-Enter your Sandbox account https://app.sandbox.dea.ga.gov.au/hub/login?next=%2Fhub%2F
- Enter the Gitlab public repository https://gitlab.com/usyd_hydrolab/teaching/summerschool_2022
- download the repo as a zip and save that zip file in your computer
- drag and drop the zip file in the sandbox
- in sandbox: file | new terminal
- in the terminal type: unzip [name of file]
- you will find the file in the sandbox directory. Start exploring
