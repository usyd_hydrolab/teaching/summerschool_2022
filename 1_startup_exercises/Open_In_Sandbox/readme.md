Instructions:

This introduction is to be run in the DEA sandbox. 

1. Enter your Sandbox account https://app.sandbox.dea.ga.gov.au/hub/login?next=%2Fhub%2F

2. Enter the Gitlab public repository https://gitlab.com/usyd_hydrolab/teaching/summerschool_2022

3. download the repo as a zip and save that zip file in your computer

4. drag and drop the zip file in the sandbox

5. in sandbox: file | new terminal

6. in the terminal type: unzip [name of file]

7. you will find the file in the sandbox directory. Start exploring


