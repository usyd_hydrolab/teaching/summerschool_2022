{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Xarray \n",
    "source: modified from Xarray tutorials and DEA beginners guide\n",
    "\n",
    "## Background\n",
    "`Xarray` is a python library which simplifies working with **labelled** multi-dimension arrays. `Xarray` introduces labels in the forms of dimensions, coordinates and attributes on top of raw `numpy` arrays, allowing for more intitutive and concise development. Payoff: **you’ll write less code & you’ll understand what you were thinking** when you come back to look at it weeks or months later.\n",
    "\n",
    "## Data structures for multi-dimensional data\n",
    "\n",
    "Multi-dimensional (a.k.a. N-dimensional, ND) arrays (sometimes called “tensors”) are an essential part of computational science. In Python, NumPy provides the fundamental data structure and API for working with raw ND arrays. However, **real-world datasets are usually more than just raw numbers**; they have **labels** which encode information about how the array values map to locations in space, time, etc.\n",
    "\n",
    "Here is an example of how we might structure a dataset for a weather forecast:\n",
    "\n",
    "![image](xarray1.png)\n",
    "\n",
    "You’ll notice multiple data variables (temperature, precipitation), coordinate variables (latitude, longitude), and dimensions (x, y, t). \n",
    "\n",
    "The N-dimensional nature of xarray’s data structures makes it suitable for dealing with multi-dimensional scientific data, and its use of **dimension names instead of axis** labels (`dim='time'` instead of `axis=0`) makes such arrays much more manageable than the raw numpy ndarray: **with xarray, you don’t need to keep track of the order of an array’s dimensions**\n",
    "\n",
    "\n",
    "### Data structures\n",
    "Xarray provides two data structures: the `DataArray` and `Dataset`. The `DataArray` class attaches dimension names, coordinates and attributes to multi-dimensional arrays while `Dataset` combines multiple arrays.\n",
    "\n",
    "Let's start creating some data :)\n",
    "\n",
    "#### 1. DataArray\n",
    "let’s create a `DataArray` named a with three dimensions (named `x`, `y`, `z`) from a `numpy` array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load packages\n",
    "import numpy as np\n",
    "import xarray as xr\n",
    "\n",
    "# create the DataArray\n",
    "da = xr.DataArray(\n",
    "    np.ones((3, 4, 2)),\n",
    "    dims=(\"x\", \"y\", \"z\"),\n",
    "    name=\"test\",   # array’s name\n",
    "    coords={\"x\": [1,2,3], \"y\": [-1, 0, 1, 2],  \"z\": [3,6]},  \n",
    "    attrs={\"attr\": \"value\"},\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "we used a 3x4 `numpy` array with all values being equal to `1`.\n",
    "\n",
    "We also passed a sequence (a tuple here, but could also be a list) containing the **dimension** names `x`, `y`, and `z` to `dims`. \n",
    "\n",
    "`coords` is a dict-like container of arrays (**coordinates**) that label each point.\n",
    "\n",
    "We can also attach arbitrary metadata (**attributes**) to the `DataArray` by passing a dict-like to the `attrs` parameter.\n",
    "\n",
    "#### Now that we have the DataArray we can look at its string representation.\n",
    "\n",
    "xarray has two representation types: `\"html\"` (which is only available in notebooks) and `\"text\"`. To choose between them, use the `display_style` option."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# text representation:\n",
    "with xr.set_options(display_style=\"text\"):\n",
    "    display(da)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It consists of:\n",
    "\n",
    "* the **name** of the `DataArray` (`'test'`). \n",
    "\n",
    "* the **dimensions** of the array `(x: 3, y: 4, z: 2)`: this tells us that the first dimension is named `x` and has a size of `3`, the second dimension is named `y` and has a size of `4`, and the third dimension is named `z` and has a size of `2`\n",
    "\n",
    "* a preview of the data (remember: np.ones((3, 4, 2)))\n",
    "\n",
    "* a (unordered) list of **coordinates** or dimensions with coordinates with one item per line. Each item has a name, one or more dimensions in parentheses, a dtype and a preview of the values. Also, if it is a dimension coordinate, it will be marked with a `*`.\n",
    "\n",
    "* a (unordered) list of attributes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# html representation:\n",
    "with xr.set_options(display_style=\"html\"):\n",
    "    display(da)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "it looks similar except the data preview was collapsed to a single line (we can expand it by clicking on the symbol on the left)\n",
    "\n",
    "Once we have created the `DataArray`, we can look at its data, dims, coords, attrs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "da.data\n",
    "# Exercise: try with the rest (dims, coords, attrs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2. Dataset\n",
    "`Dataset` objects collect multiple data variabless.\n",
    "\n",
    "The constructor of `Dataset` takes three parameters:\n",
    "\n",
    "* `data_vars`: dict-like mapping names to values. It has the format described in coordinates except we need to use either `DataArray` objects or the tuple syntax since we have to provide dimensions\n",
    "\n",
    "* `coords`: same as for DataArray\n",
    "\n",
    "* `attrs`: same as for DataArray\n",
    "\n",
    "To explore `xarray`  `Dataset` we have a file containing some surface reflectance data extracted from the DEA platform. \n",
    "The object that we get `ds` is a `xarray` `Dataset`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(\"example_netcdf.nc\")\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* **dimension** `time` has a size of 12, dim `y` has a size of 601, dim `x` has a size of 483\n",
    "\n",
    "* **coordinates** `time`, `y` ,`x` and their datatype\n",
    "\n",
    "* **variables** `red`, `green`, `blue` each has the same format as the coordinates\n",
    "\n",
    "* **attributes** "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To access a variable we can access as if it were a Python dictionary, or using the `.` notation, which is more convenient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"green\"]\n",
    "# Or:\n",
    "ds.green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dimensions are also stored as numeric arrays that we can easily access:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"time\"]\n",
    "# Or:\n",
    "ds.time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Metadata is referred to as attributes and is internally stored under `.attrs`, but the same convenient `.` notation applies to them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.attrs[\"crs\"]  # coordinate reference system\n",
    "# Or:\n",
    "ds.crs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`DataArrays` store their data internally as multidimensional `numpy` arrays. \n",
    "But these arrays contain dimensions or labels that make it easier to handle the data. \n",
    "To access the **underlaying numpy array** of a `DataArray` we can use the `.values` notation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arr = ds.green.values\n",
    "\n",
    "type(arr), arr.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "remember *shape* from Numpy: (`time`: 12, `y`: 601, `x`: 483 )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing\n",
    "\n",
    "`Xarray` offers two different ways of selecting data. This includes the `isel()` approach, where data can be selected based on its **index** (like `numpy`).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ds.time.values)\n",
    "\n",
    "ss = ds.green.isel(time=0)\n",
    "ss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or the `sel()` approach, used for selecting data based on its dimension of **label value**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss = ds.green.sel(time=\"2018-01-08\")\n",
    "ss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Slicing data is also used to **select a subset of data**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shows the array’s data as a numpy.ndarray.\n",
    "# in this case shows the first 10 1D array values extracted from coordinate x  \n",
    "ss.x.values[0:10]\n",
    "# Remember: `values` To access the **underlaying numpy array** of a `DataArray`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss.x.values[100]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the dimension names, we do not have to rely on dimension order and can use them explicitly to slice data.\n",
    "\n",
    "'Slice' (start, stop, or ‘step`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss = ds.green.sel(time=\"2018-01-08\", x=slice(2378390, 2380390))\n",
    "ss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Xarray exposes lots of functions to easily transform and analyse `Datasets` and `DataArrays`. \n",
    "For example, to calculate the spatial mean, standard deviation or sum of the green band:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mean of green band:\", ds.green.mean().values)\n",
    "print(\"Standard deviation of green band:\", ds.green.std().values)\n",
    "print(\"Sum of green band:\", ds.green.sum().values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting data with Matplotlib\n",
    "Plotting is also conveniently integrated in the library. \n",
    "Run %matplotlib inline, which ensures figures plot correctly in the Jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "identify the band to plot. In this example, the green satellite band is used. \n",
    "\n",
    "To make a plot for a single timestep only, select the desired timestep using either `isel` or `sel`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"green\"].isel(time=0).plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### exercise:\n",
    "Explore the time coordinates and use `sel` to select and plot a desired timestep in the \"red\" band"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are plotting a rbg image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[[\"red\", \"green\", \"blue\"]].isel(time=0).to_array().plot.imshow(robust=True, figsize=(6, 6))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rgb = ds[[\"red\", \"green\", \"blue\"]].isel(time=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rgb.to_array().plot.imshow(robust=True, figsize=(6, 6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To plot a single band, the data must be an `xarray.DataArray`\n",
    "\n",
    "\n",
    "##### Plotting multiple timesteps\n",
    "It is often useful to produce plots for a single measurement across time, for example to compare change between satellite observations or summary datasets. To plot multiple images, skip the `isel()` step above and plot the entire `xarray.DataArray` directly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.green.plot(col=\"time\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plots above are dark and difficult to see clearly. To improve the appearance of `xarray` plots, use the `robust=True` argument to optimise the plot colours by clipping extreme values or outliers. This will use the 2nd and 98th percentiles of the data to compute the color limits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.green.plot(col=\"time\", robust=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plots can be further customised by adding custom colour maps/styles using the `cmap` parameter.\n",
    "Some best-practice perceptually uniform colour maps include:\n",
    "\n",
    "\"viridis\", \"plasma\", \"inferno\", \"magma\", \"cividis\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise:\n",
    "\n",
    "Open the image, examine and plot:\n",
    "* Sea surface temperature from \"Sea surface temperatures collected by PCMDI for use by the IPCC.\". File: tos_O1_2001-2002.nc\n",
    "* precipitation flux, air temperature: \"From the Community Climate System Model (CCSM), one time step of precipitation flux, air temperature, and eastward wind.\" File: sresa1b_ncar_ccsm3-example.nc\n",
    "\n",
    "[File source](https://www.unidata.ucar.edu/software/netcdf/examples/files.html)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
