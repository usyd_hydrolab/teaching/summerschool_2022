{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e78d614a-34b1-4f08-ab5c-345e36b13d67",
   "metadata": {},
   "source": [
    "## Introduction to Numpy\n",
    "### Background\n",
    "`Numpy` is a Python library which adds support for large, multi-dimension arrays and metrics, along with a large collection of high-level mathematical functions to operate on these arrays. \n",
    "\n",
    "The core concept in `numpy` is the \"array\" which is *equivalent to lists of numbers but can be multidimensional*. \n",
    "\n",
    "#### What’s the difference between a Python list and a NumPy array?\n",
    "**Remember**: Python list = an ordered sequence of items (any type). `a = [1, 2.2, 'python']` int, float, string\n",
    "NumPy gives you an enormous range of fast and efficient ways of creating arrays and manipulating numerical data inside them. While a Python list can contain different data types within a single list, all of the **elements in a NumPy array should be homogeneous**. The mathematical operations that are meant to be performed on arrays would be extremely inefficient if the arrays weren’t homogeneous.\n",
    "\n",
    "**Why use NumPy?**\n",
    "\n",
    "NumPy arrays are *faster* and more compact than Python lists. An array consumes *less memory* and is convenient to use. NumPy uses much less memory to store data and it provides a mechanism of *specifying the data types*. This allows the code to be optimized even further.\n",
    "\n",
    "#### What is an array?\n",
    "An array is a central data structure of the NumPy library. An array is a *grid of values* and it contains information about the raw data, how to locate an element, and how to interpret an element. It has a grid of elements that can be indexed in various ways. The elements are all of the same type, referred to as the array `dtype`.\n",
    "\n",
    "An array can be indexed by a tuple (immutable) of nonnegative integers, by booleans, by another array, or by integers. The `rank` of the array is the number of dimensions. The `shape` of the array is a tuple of integers giving the size of the array along each dimension.\n",
    "\n",
    "## Let's get started!\n",
    "\n",
    "To access NumPy and its functions *import* it in your Python code like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fba9d1aa-e311-4519-9bcc-f49788411c59",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b76d9832",
   "metadata": {},
   "source": [
    "We shorten the imported name to `np` for better readability of code using NumPy. This is a widely adopted convention that you should follow so that anyone working with your code can easily understand it.\n",
    "\n",
    "To declare a `numpy` array we do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b216213",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([1, 2, 3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c77a780",
   "metadata": {},
   "source": [
    "By default the result of a *function* or operation is shown underneath the cell containing the code. \n",
    "\n",
    "If we want to reuse this result for a later operation we can assign it to a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf1b7c1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3, 4, 5, 6])\n",
    "print(\"a:\", a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "239fff9e",
   "metadata": {},
   "source": [
    "Besides creating an array from a sequence of elements, you can easily create an array filled with 0’s or 1's. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e205e36f",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.zeros(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48d71f3a",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.ones(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8af2dd7",
   "metadata": {},
   "source": [
    "You can create an array with a range of elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed3366a5",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.arange(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f95b5153",
   "metadata": {},
   "source": [
    "And even an array that contains a range of evenly spaced intervals. To do this, you will specify the **first number**, **last number**, and the **step size**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d284702",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.arange(2, 9, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba92f734",
   "metadata": {},
   "source": [
    "While the default data type is floating point (`np.float64`), you can explicitly specify which data type you want using the `dtype` keyword. (More info on **data types** is coming ahead)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79e929d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.ones(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "175743ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.ones(2, dtype=np.int64)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0364af58",
   "metadata": {},
   "source": [
    "One way we can initialize NumPy arrays is from Python lists (ordered sequence of items), using nested lists for **two- or higher-dimensional data**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ceb0336-10b1-484a-a912-739409324d8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])\n",
    "print(\"b:\",b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b073b875-2b17-4ed7-a69c-683b2133c058",
   "metadata": {},
   "source": [
    "#### Basic array operations\n",
    "Most of the functions and operations defined in numpy can be applied to arrays. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbd206cc-1678-4f03-85ac-63a161fdd100",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr1 = np.array([1, 2, 3, 4])\n",
    "arr2 = np.array([3, 4, 5, 6])\n",
    "\n",
    "np.add(arr1, arr2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8398cca-5038-414e-8191-9ece341a2bd9",
   "metadata": {},
   "source": [
    "We can also do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f6ce6c8-2df4-4214-a387-638a0f07f8ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = arr1 + arr2\n",
    "print(a)\n",
    "b = arr1 - arr2\n",
    "print(b)\n",
    "c = arr1 * arr2\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b30bdb0",
   "metadata": {},
   "source": [
    "### More information about arrays\n",
    "You might occasionally hear an array referred to as a “ndarray,” which is shorthand for “N-dimensional array.” An N-dimensional array is simply an array with any number of dimensions. You might also hear **1-D**, or one-dimensional array, **2-D**, or two-dimensional array, and so on. The NumPy `ndarray` class is used to represent both matrices and vectors. A **vector** is an array with a single dimension (there’s no difference between row and column vectors), while a **matrix** refers to an array with two dimensions. For **3-D** or higher dimensional arrays, the term **tensor** is also commonly used.\n",
    "\n",
    "#### Array attributes\n",
    "Array attributes reflect information intrinsic to the array itself. An array is usually a fixed-size container of items of the same type and size. The number of dimensions and items in an array is defined by its **shape**. The shape of an array is a tuple (ordered sequence of items (immutable, cant' change it's value)) of non-negative integers that specify the sizes of each dimension.\n",
    "\n",
    "![Image](multidimensional_array.png)\n",
    "\n",
    "In NumPy, dimensions are called **axes**. This means that if you have a 2D array that looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f58bb62b",
   "metadata": {},
   "outputs": [],
   "source": [
    "[[0., 0., 0.],\n",
    " [1., 1., 1.]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "becff9a1",
   "metadata": {},
   "source": [
    "Our array has 2 axes. The first axis has a length of 2 and the second axis has a length of 3.\n",
    "The contents of an array can be accessed and modified by indexing or slicing the array. But before looking into indexing and slicing, let's first explore multidimensional arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f6c5a0a-4da3-4772-aed0-fcaf721de7bd",
   "metadata": {},
   "source": [
    "### Multi-dimensional arrays\n",
    "\n",
    "#### How do you know the shape and size of an array?\n",
    "`ndarray.ndim` will tell you the number of axes, or dimensions, of the array.\n",
    "\n",
    "`ndarray.size` will tell you the total number of elements of the array. This is the product of the elements of the array’s shape.\n",
    "\n",
    "`ndarray.shape` will display a tuple of integers that indicate the number of elements stored along each dimension of the array. If, for example, you have a 2-D array with 2 rows and 3 columns, the shape of your array is (2, 3).\n",
    "\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "146e10a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "array_example = np.array([[[0, 1, 2, 3],\n",
    "                           [4, 5, 6, 7]],\n",
    "\n",
    "                          [[0, 1, 2, 3],\n",
    "                           [4, 5, 6, 7]],\n",
    "\n",
    "                          [[0 ,1 ,2, 3],\n",
    "                           [4, 5, 6, 7]]])\n",
    "\n",
    "array_example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e73d2a03",
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of dimensions:\n",
    "print(array_example.ndim)\n",
    "# total number of elements:\n",
    "print(array_example.size)\n",
    "# shape of the array\n",
    "print(array_example.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "668eb29e-ef92-4240-afa2-7196df9883fe",
   "metadata": {},
   "source": [
    "Any array can be **reshaped** into different shapes using the function `reshape`. For example, you can reshape this 1D array with 6 elements to an 2D array with 2 rows and 3 columns\n",
    "\n",
    "![image](reshape.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d962ab82",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.arange(1,7)  \n",
    "print(\"a:\", a)\n",
    "b1 = a.reshape(2, 3)\n",
    "print(\"b1:\", b1)\n",
    "b2 = b1.reshape(3, 2)\n",
    "print(\"b2:\", b2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e25fa8ea",
   "metadata": {},
   "source": [
    "We can reshape this 2D array with 24 elements distributed in 3 rows and 8 columns to a 3D array with 3 rows and 4 columns and an extra dimension repeating the shape (3 rows and 4 columns). You can visualise the last array as an satellite image with 2 bands of 3*4 pixels each"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea72a482",
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.arange(24).reshape(3,8)\n",
    "print(\"b:\", b)\n",
    "c = b.reshape(2,3,4)\n",
    "print(\"c:\", c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1295802-399a-40f5-a786-e135269e0c8e",
   "metadata": {},
   "source": [
    "#### Indexing and slicing\n",
    "You may want to take a section of your array or specific array elements to use in further analysis or additional operations. To do that, you’ll need to subset, slice, and/or index your arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90854e41",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1,2,3,4])\n",
    "b = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])\n",
    "print(\"a:\",a)\n",
    "print(\"b:\",b)\n",
    "\n",
    "print(\"the first element of a is:\", a[0])\n",
    "print(\"the first element of b is:\", b[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6a6f6f8",
   "metadata": {},
   "source": [
    "Here is an example if you want to select values from your array that fulfill certain conditions, say all of the values in the array that are less than 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebe797d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "five_up = (b >= 5) #  return boolean values that specify whether or not the values in an array fulfill a certain condition.\n",
    "print(five_up) \n",
    "\n",
    "print(b[b>=5])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70617a23-7d19-4830-a2f9-30aef99d4c8c",
   "metadata": {},
   "source": [
    "**Booleans**\n",
    "There is a binary type in numpy called boolean which encodes `True` and `False` values. Boolean types are quite handy for indexing and selecting parts of images. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04da247d",
   "metadata": {},
   "source": [
    "You can select elements that are divisible by 2:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c891b3fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "divisible_by_2 = (b%2==0)\n",
    "print(divisible_by_2)\n",
    "\n",
    "divisible_by_2 = b[b%2==0]\n",
    "print(divisible_by_2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "353187c9",
   "metadata": {},
   "source": [
    "We can get subsets of the arrays using the indexing notation which is `[start:end:stride]`. Let's see what this means:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3764f717-41e9-483c-841f-a594a6b351b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])\n",
    "\n",
    "print(\"6th element in the array:\", arr[5])\n",
    "print(\"6th element to the end of array\", arr[5:])\n",
    "print(\"start of array to the 5th element\", arr[:5])\n",
    "print(\"every second element\", arr[::2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66008b76-e36b-4e02-812b-c6132dfefa0c",
   "metadata": {},
   "source": [
    "Try experimenting with the indices to understand the meaning of `start`, `end` and `stride`. What happens if you don't specify a start? What value does numpy uses instead? Note that numpy indexes start on `0`, the same convention used in Python lists.\n",
    "\n",
    "Indexes can also be negative, meaning that you start counting from the end. For example, to select the last 2 elements in an array we can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29ae6ed8-4732-4589-a196-b06194fa2691",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr[-2:]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e6a3d16-8f9d-434f-abe5-184d103f9458",
   "metadata": {},
   "source": [
    "### Arithmetic operations\n",
    "Numpy has many useful arithmetic functions. Below we demonstrate a few of these, such as mean, standard deviation and sum of the elements of an array. These operation can be performed either across the entire array, or across a specified dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e17743b-11f5-4099-ba2e-0c8a487bf8ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr = np.arange(9).reshape((3, 3))\n",
    "print(arr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd85eebe-8642-42d2-814f-4e46096814d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mean of all elements in the array:\", np.mean(arr))\n",
    "print(\"Std dev of all elements in the array:\", np.std(arr))\n",
    "print(\"Sum of all elements in the array:\", np.sum(arr))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1cc4d5a7-2af3-4272-ae44-0ef886636c52",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Sum of elements in array axis 0:\", np.sum(arr, axis=0))\n",
    "print(\"Sum of elements in array axis 1:\", np.sum(arr, axis=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfe0ede3-4ccf-4176-8445-113c986006c1",
   "metadata": {},
   "source": [
    "### Numpy data types\n",
    "Numpy arrays can contain numerical values of different types. These types can be divided in these groups:\n",
    "\n",
    "* Integers\n",
    "\n",
    "-- Unsigned (An unsigned data type simply means that the data type will only hold positive values)\n",
    "\n",
    "8 bits: `uint8`\n",
    "\n",
    "16 bits: `uint16`\n",
    "\n",
    "32 bits: `uint32`\n",
    "\n",
    "64 bits: `uint64`\n",
    "\n",
    "-- Signed\n",
    "\n",
    "8 bits: `int8`\n",
    "\n",
    "16 bits: `int16`\n",
    "\n",
    "32 bits: `int32`\n",
    "\n",
    "64 bits: `int64`\n",
    "\n",
    "* Floats\n",
    "\n",
    "32 bits: `float32`\n",
    "\n",
    "64 bits: `float64`\n",
    "\n",
    "We can specify the type of an array when we declare it, or change the data type of an existing one with the following expressions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b60e542d-df20-475a-9911-28af995c5294",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set datatype when declaring array\n",
    "arr = np.arange(5, dtype=np.uint8)\n",
    "print(\"Integer datatype:\", arr)\n",
    "\n",
    "arr = arr.astype(np.float32)\n",
    "print(\"Float datatype:\", arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5405a858-5909-4872-9a54-9cc3c48bcfa2",
   "metadata": {},
   "source": [
    "### Broadcasting\n",
    "There are times when you might want to carry out an operation between an **array** and a **single number** (also called an *operation between a vector and a scalar*) or between **arrays of two different sizes**. For example, your array (we’ll call it “data”) might contain information about distance in miles but you want to convert the information to kilometers. You can perform this operation with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e29b14cc-647b-4b0b-a08a-a72dcbbf35f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.zeros((3, 3))\n",
    "print(a)\n",
    "\n",
    "a = a + 1\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0115e51b",
   "metadata": {},
   "source": [
    "what happened? \n",
    "\n",
    "![Image](broadcasting1.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a9c0e97-b173-40b1-acce-58b4be5bcb1c",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.arange(9).reshape((3, 3))\n",
    "print(\"c:\",a)\n",
    "b = np.arange(3)\n",
    "print(\"b:\",b)\n",
    "c = a + b\n",
    "print(\"c:\",c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "690c9a5b",
   "metadata": {},
   "source": [
    "More information on broadcasting can be found [here](https://numpy.org/doc/stable/user/basics.broadcasting.html) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cc49b9a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optional Exercise\n",
    "# 1. Create a 1D array\n",
    "# 2. Return values greater than 7\n",
    "# 3. reshape , the 1D into a 3D array.\n",
    "# 4. Estimate the sum of all elements:\n",
    "# a. elements in array axis 0\n",
    "# b. elements in the entire array"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
