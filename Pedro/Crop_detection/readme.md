### Please install the conda environment and activate it according to the instructions on the manual.

 - After please run the following command on your terminal to install the requests package:
conda install -c anaconda requests

 - Then replace the your project folder with the updated one, or replace the files in your current folder.

 - In your project folder run: jupyter notebook
