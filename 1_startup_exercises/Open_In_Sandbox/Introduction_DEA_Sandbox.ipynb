{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "17c20b17-2116-4234-ae1a-b39335c7c6e1",
   "metadata": {},
   "source": [
    "# Open Data Cube\n",
    "The Open Data Cube provides an **integrated gridded data analysis environment** for decades of **analysis ready** earth observation satellite and related **data** from multiple satellite and other acquisition systems.\n",
    "\n",
    "## Overview\n",
    "The Open Data Cube is a collection of software designed to:\n",
    "\n",
    "* Catalogue large amounts of Earth Observation data\n",
    "\n",
    "* Provide a Python based API (Application Programming Interface) for high performance querying and data access\n",
    "\n",
    "* Give scientists and other users easy ability to perform Exploratory Data Analysis\n",
    "\n",
    "* Allow scalable continent scale processing of the stored data\n",
    "\n",
    "* Track the provenance of all the contained data to allow for quality control and updates\n",
    "\n",
    "The Open Data Cube software is based around the **datacube-core library**, an open source Python library, released under the Apache 2.0 license.\n",
    "\n",
    "## Existing Deployments\n",
    "**Digital Earth Australia**\n",
    "* Digital Earth Africa\n",
    "* Swiss Data Cube\n",
    "* Vietnam Open Data Cube\n",
    "\n",
    "## Jupyter Notebooks\n",
    "For interactively exploring data in a Data Cube, we recommend using Jupyter Notebooks.\n",
    "GitHub repositories of example Open Data Cube notebooks:\n",
    "https://github.com/GeoscienceAustralia/dea-notebooks/ \n",
    "\n",
    "## Sandbox \n",
    "Sandbox's public “tier” offers 2 cores, with 16GB of RAM and 10GB of storage.\n",
    "\n",
    "Note: in case a package is missing in sandbox, you can install it:\n",
    "* in a terminal:\n",
    "```pip install [package name]```\n",
    "* or in a code cell in a notebook by:\n",
    "```!pip install  [package name]```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02f9fe1c-847e-4ad5-8896-106253fb7e30",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"hello summerschool\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab50fa16-1da7-40cf-bc12-d5a17f855777",
   "metadata": {},
   "source": [
    "# DEA\n",
    "Digital Earth Australia (DEA) is a **digital platform that catalogues large amounts of Earth observation data** covering continental Australia. Deployment of the Open Data Cube (ODC).\n",
    "\n",
    "##  Satellite datasets in DEA\n",
    "Digital Earth Australia catalogues data from a range of satellite sensors. The earliest datasets of optical satellite imagery in DEA date from 1986. DEA includes data from:\n",
    "\n",
    "* Landsat 5 TM (LS5 TM), operational between March 1984 and January 2013\n",
    "* Landsat 7 ETM+ (LS7 ETM+), operational since April 1999\n",
    "* Landsat 8 OLI (LS8 OLI), operational since February 2013\n",
    "* Sentinel 2A MSI (S2A MSI), operational since June 2015\n",
    "* Sentinel 2B MSI (S2B MSI, operational since March 2017\n",
    "\n",
    "## Analysis Ready Data\n",
    "Digital Earth Australia produces **Analysis Ready Data (ARD)** for each of the sensors of satelites listed above. The ARD standard for satellite data requires that data have undergone a number of **processing steps**, along with the creation of **additional attributes** for the data. DEA's ARD datasets include the following characteristics:\n",
    "\n",
    "* **Geometric correction:** This includes establishing ground position, accounting for terrain (orthorectification) and ground control points, and assessing absolute position accuracy. Geometric calibration means that imagery is positioned accurately on the Earth's surface and stacked consistently so that sequential observations can be used to track meaningful change over time. Adjustments for ground variability typically use a Digital Elevation Model (DEM).\n",
    "* **Surface reflectance correction:** This includes adjustments for sensor/instrument gains, biases and offsets, include adjustments for terrain illumination and sensor viewing angle with respect to the pixel position on the surface. Once satellite data is processed to surface reflectance, pixel values from the same sensor can be compared consistently both spatially and over time.\n",
    "* **Observation attributes:** Per-pixel metadata such as quality flags and content attribution that enable users to make informed decisions about the suitability of the products for their use. For example, clouds, cloud shadows, missing data, saturation and water are common pixel level attributes.\n",
    "* **Metadata:** Dataset metadata including the satellite, instrument, acquisition date and time, spatial boundaries, pixel locations, mode, processing details, spectral or frequency response and grid projection.\n",
    "\n",
    "## Derived products\n",
    "\n",
    "In addition to ARD satellite data, DEA generates a range of products that are derived from Landsat or Sentinel-2 surface reflectance data. These products have been developed to characterise and monitor different aspects of Australia's natural and built environment, such as mapping the distribution of water and vegetation across the landscape through time. Derived DEA products include:\n",
    "\n",
    "* Water Observations from Space (WOfS): WOfS is the world's first continent-scale map of surface water and provides images and data showing where water has been seen in Australia from 1987 to the present. This map can be used to better understand where water usually occurs across the continent and to plan water management strategies.\n",
    "\n",
    "* Fractional Cover (FC): Fractional Cover (FC) is a measurement that splits the landscape into three parts, or fractions; green (leaves, grass, and growing crops), brown (branches, dry grass or hay, and dead leaf litter), and bare ground (soil or rock). DEA uses Fractional Cover to characterise every 25 m square of Australia for any point in time from 1987 to today. This measurement can inform a broad range of natural resource management issues.\n",
    "\n",
    "* High and Low Tide Composites (HLTC): The High and Low Tide Composites (HLTC) are imagery mosaics developed to visualise Australia's coasts, estuaries and reefs at low and high tide, whilst removing the influence of noise features such as clouds, breaking water and sun-glint. These products are highly interpretable, and provide a valuable snapshot of the coastline at different biophysical states.\n",
    "\n",
    "* Intertidal Extents Model (ITEM): The Intertidal Extents Model (ITEM) product utilises 30 years of Earth observation data from the Landsat archive to map the extents and topography of Australia's intertidal mudflats, beaches and reefs; the area exposed between high and low tide.\n",
    "\n",
    "* National Intertidal Digital Elevation Model (NIDEM): The National Intertidal Digital Elevation Model (NIDEM) is a national dataset that maps the three-dimensional structure of Australia’s intertidal zone. NIDEM provides a first-of-its kind source of intertidal elevation data for Australia’s entire coastline."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dabc3831-1d6f-45a4-a8ab-e9c39275153e",
   "metadata": {},
   "source": [
    "# Products and measurements\n",
    "The DEA datacube contains both **raw satellite data and derivative data \"products\"**. These data products are often composed of a range of \"measurements\" such as the suite of remote sensing band values or statistical product summaries. \n",
    "\n",
    "## Load packages & connect to the datacube\n",
    "The datacube package is required to access and work with available data. The pandas package is required to format tables. The DcViewer utility provides an interface for interactively exploring the products available in the datacube."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e4f5565-b49a-4fc7-818a-10b08c0fe461",
   "metadata": {},
   "outputs": [],
   "source": [
    "import datacube\n",
    "import pandas as pd\n",
    "from odc.ui import DcViewer\n",
    "\n",
    "# Set some configurations for displaying tables nicely\n",
    "pd.set_option(\"display.max_colwidth\", 200)\n",
    "pd.set_option(\"display.max_rows\", None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7855d8f8-fb38-4e18-8438-834b29e0b323",
   "metadata": {},
   "source": [
    "After importing the datacube package, users need to specify a name for their session, known as the app name.\n",
    "This name is generated by the user and is used to track down issues with database queries. It does not have any effect on the analysis.\n",
    "The resulting dc object provides access to all the data contained within the Digital Earth Australia datacube."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53c5660b-f54b-40bf-9c99-fd9bfa7be0be",
   "metadata": {},
   "outputs": [],
   "source": [
    "dc = datacube.Datacube(app=\"summerschool_introduction_Sandbox\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f0bc03a-615c-4e07-ba13-2f0dd706f25e",
   "metadata": {},
   "source": [
    "### List products\n",
    "The following cell lists all products that are currently available in the DEA datacube by using the dc.list_products() function.\n",
    "\n",
    "Products listed under name in the following table represent the product options available when querying the datacube. The table below provides some useful information about each product, including a brief product description, the data's license, and the product's default crs (coordinate reference system) and resolution if applicable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "198d6bb1-4a59-489d-8fd8-cd18fc6d3534",
   "metadata": {},
   "outputs": [],
   "source": [
    "products = dc.list_products()\n",
    "products"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae7ff815-b49e-407b-a7fb-0e64ecdf4d12",
   "metadata": {},
   "source": [
    "### List measurements\n",
    "Most products are associated with a range of available measurements. These can be individual satellite bands (e.g. Landsat's near-infrared band) or statistical product summaries.\n",
    "\n",
    "The `dc.list_measurements()` function can be used to interrogate the measurements associated with a given product (specified by the name column from the table above). For example, `ga_ls5t_ard_3` refers to the Geoscience Australia Landsat 5 Analysis-ready data Collection 3 product.\n",
    "\n",
    "The table below includes a range of technical information about each band in the `ga_ls5t_ard_3` dataset, including any aliases which can be used to load the data, the data type or dtype, any flags_definition that are associated with the measurement (this information is used for tasks like cloud masking), and the measurement's nodata value.\n",
    "\n",
    "Change the `product` name below and re-run the following cell to explore available measurements associated with other products."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de6a87ea-6305-493d-9df7-c0985e5ad208",
   "metadata": {},
   "outputs": [],
   "source": [
    "product = \"ga_ls5t_ard_3\"\n",
    "\n",
    "measurements = dc.list_measurements()\n",
    "measurements.loc[product]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33c732dd-02e8-4a11-bf87-85bdcd1f250d",
   "metadata": {},
   "source": [
    "### DEA Explorer sites\n",
    "Another way to view the available data within a datacube is to visit the DEA Datacube Explorer sites. These webpages visualise the data that is available for every product in DEA.\n",
    "[DEA Sandbox Datacube Explorer](https://explorer.sandbox.dea.ga.gov.au/products)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bddd6e3-18ee-4753-94a2-a394800f00c2",
   "metadata": {},
   "source": [
    "##  loading data\n",
    "Loading data from the Digital Earth Australia (DEA) instance of the Open Data Cube requires the construction of a data query that specifies the what, where, and when of the data request. Each query returns a [multi-dimensional xarray object](https://xarray.pydata.org/en/stable/) containing the contents of your query. It is essential to understand the xarray data structures as they are fundamental to the structure of data loaded from the datacube. Manipulations, transformations and visualisation of xarray objects provide datacube users with the ability to explore and analyse DEA datasets, as well as pose and answer scientific questions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8be4d25e-3a0c-46c5-a112-15dd23cb7900",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Load packages\n",
    "from odc.ui import with_ui_cbk # enables a progress bar when loading large amounts of data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf99753a-ad49-4647-baf0-da6bd6fc368b",
   "metadata": {},
   "source": [
    "### Loading data using `dc.load()`\n",
    "Loading data from the datacube uses the `dc.load()` function.\n",
    "\n",
    "The function requires the following minimum arguments:\n",
    "\n",
    "* `product`: The data product to load (to revise DEA products, see the Products and measurements notebook).\n",
    "* `x`: The spatial region in the x dimension. By default, the x and y arguments accept queries in a geographical co-ordinate system WGS84, identified by the EPSG code 4326.\n",
    "* `y`: The spatial region in the y dimension. The dimensions longitude/latitude and x/y can be used interchangeably.\n",
    "* `time`: The temporal extent. The time dimension can be specified using a tuple of datetime objects or strings in the \"YYYY\", \"YYYY-MM\" or \"YYYY-MM-DD\" format.\n",
    "* `crs`: coordinate reference system\n",
    "For example, to load 2020 data from the Landsat 8 NBAR-T annual geomedian product, use the following parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53ad2c6d-f9a0-44d1-991a-16d7daf4523d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = dc.load(product=\"ls8_nbart_geomedian_annual\",\n",
    "             x=(144.887, 144.988),\n",
    "             y=(-35.995, -35.895),\n",
    "             time=(\"2020-01-01\", \"2020-12-31\"),\n",
    "             output_crs= \"epsg:3577\",\n",
    "             resolution = (-30, 30))\n",
    "\n",
    "print(ds)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4f052af-a5a8-4c8f-b4ce-92c354180530",
   "metadata": {},
   "source": [
    "### Interpreting the resulting xarray.Dataset\n",
    "The variable `ds` has returned an `xarray.Dataset` containing all data that matched the spatial and temporal query parameters inputted into `dc.load`.\n",
    "\n",
    "*Dimensions*\n",
    "\n",
    "* This header identifies the number of timesteps returned in the search (time: 1) as well as the number of pixels in the `x` and `y` directions of the data query.\n",
    "\n",
    "*Coordinates*\n",
    "\n",
    "* `time` identifies the date attributed to each returned timestep.\n",
    "* `x` and `y` are the coordinates for each pixel within the spatial bounds of the query.\n",
    "\n",
    "*Data variables*\n",
    "\n",
    "* These are the measurements available for the nominated product. For every date (`time`) returned by the query, the measured value at each pixel (`y`, `x`) is returned as an array for each measurement. Each data variable is itself an `xarray.DataArray` object.\n",
    "\n",
    "*Attributes*\n",
    "\n",
    "* `crs` identifies the coordinate reference system (CRS) of the loaded data. \n",
    "\n",
    "### Inspecting an individual xarray.DataArray\n",
    "The `xarray.Dataset` loaded above is itself a collection of individual `xarray.DataArray` objects that hold the actual data for each data variable/measurement. For example, all measurements listed under Data variables above (e.g. `blue, green, red, nir, swir1, swir2`) are `xarray.DataArray` objects.\n",
    "\n",
    "These `xarray.DataArray objects` can be inspected or interacted with by using either of the following syntaxes:\n",
    "\n",
    "`ds[\"measurement_name\"]`\n",
    "or\n",
    "\n",
    "`ds.measurement_name`\n",
    "The ability to access individual variables means that these can be directly viewed, or further manipulated to create new variables. For example, run the following cell to access data from the near infra-red satellite band (i.e. `nir`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b80f737-5a9e-4a91-834b-0f1227de7f86",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.nir"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afb8ecac-cf49-4275-98bc-d9621025217f",
   "metadata": {},
   "source": [
    " the object header informs us that it is an `xarray.DataArray` containing data for the `nir` satellite band.\n",
    "\n",
    "Like an `xarray.Dataset`, the array also includes information about the data's dimensions (i.e. `(time: 1, y: 399, x: 340)`), coordinates and attributes. This particular data variable/measurement contains some additional information that is specific to the `nir` band, including details of array's nodata value (i.e. `nodata: -999`).\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfc8d2b3-1f8a-4ec8-ae42-9c50ae6d11cc",
   "metadata": {},
   "source": [
    "### Customising the `dc.load()` function\n",
    "The `dc.load()` function can be tailored to refine a query.\n",
    "\n",
    "Customisation options include:\n",
    "\n",
    "*`measurements`: This argument is used to provide a list of measurement names to load, as listed in dc.list_measurements(). For satellite datasets, measurements contain data for each individual satellite band (e.g. near infrared). If not provided, all measurements for the product will be returned.\n",
    "\n",
    "*`crs`: The coordinate reference system (CRS) of the query's x and y coordinates is assumed to be WGS84/EPSG:4326 unless the crs field is supplied, even if the stored data is in another projection or the output_crs is specified. The crs parameter is required if the query's coordinates are in any other CRS.\n",
    "\n",
    "*`group_by`: Satellite datasets based around scenes can have multiple observations per day with slightly different time stamps as the satellite collects data along its path. These observations can be combined by reducing the time dimension to the day level using group_by=solar_day.\n",
    "\n",
    "*`output_crs` and `resolution`: To reproject or change the resolution the data, supply the output_crs and resolution fields.\n",
    "\n",
    "*`resampling`: This argument allows you to specify a custom spatial resampling method to use when data is reprojected into a different CRS.\n",
    "\n",
    "#### measurements\n",
    "By default, `dc.load()` will load all measurements in a product.\n",
    "\n",
    "To load data from the `red`, `green` and `blue` satellite bands only, add `measurements=[\"red\", \"green\", \"blue\"]` to the query:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48108765-9142-43e2-9cbe-01001b4b7082",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_rgb = dc.load(product=\"ls8_nbart_geomedian_annual\",\n",
    "                 measurements=[\"red\", \"green\", \"blue\"],\n",
    "                 x=(153.3, 153.4),\n",
    "                 y=(-27.5, -27.6),\n",
    "                 time=(\"2015-01-01\", \"2015-12-31\"))\n",
    "\n",
    "ds_rgb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84311639-5f16-4bae-8026-5793975fd35a",
   "metadata": {},
   "source": [
    "#### Loading data for coordinates in any CRS\n",
    "By default, `dc.load()` assumes that the queried `x` and `y` coordinates are in the `WGS84`/`EPSG:4326` CRS. If these coordinates are in a different coordinate system, specify this using the `crs` parameter.\n",
    "\n",
    "#### CRS reprojection\n",
    "Certain applications may require that data is output into a specific CRS. Data can be reprojected by specifying the new `output_crs` and identifying the `resolution` required.\n",
    "\n",
    "#### Spatial resampling methods\n",
    "When a product is re-projected to a different CRS and/or resolution, the new pixel grid may differ from the original input pixels by size, number and alignment. It is therefore necessary to apply a spatial \"resampling\" rule that allocates input pixel values into the new pixel grid.\n",
    "\n",
    "By default, `dc.load()` resamples pixel values using \"nearest neighbour\" resampling, which allocates each new pixel with the value of the closest input pixel. Depending on the type of data and the analysis being run, this may not be the most appropriate choice (e.g. for continuous data).\n",
    "\n",
    "## Loading data using the query dictionary syntax\n",
    "It is often useful to re-use a set of query parameters to load data from multiple products. To achieve this, load data using the \"query dictionary\" syntax. This involves placing the query parameters inside a Python dictionary object which can be re-used for multiple data loads:\n",
    "\n",
    "```python \n",
    "query = {\"x\": (153.3, 153.4),\n",
    "         \"y\": (-27.5, -27.6),\n",
    "         \"time\": (\"2015-01-01\", \"2015-12-31\"),\n",
    "         \"output_crs\": \"EPSG:32756\",\n",
    "         \"resolution\": (-250, 250)}\n",
    "\n",
    "ds_ls8 = dc.load(product=\"ls8_nbart_geomedian_annual\",\n",
    "                 **query)```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96bd20aa-d36a-43e2-8f29-121a5f271507",
   "metadata": {},
   "source": [
    "# previous: Introduction to numpy and Xarray"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
